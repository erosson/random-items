module Main exposing (main)

import Browser
import Model as M exposing (Flags, Model, Msg)
import View exposing (view)


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = M.init
        , update = M.update
        , subscriptions = M.subscriptions
        , onUrlChange = M.OnUrlChange
        , onUrlRequest = M.OnUrlRequest
        }
