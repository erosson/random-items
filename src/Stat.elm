module Stat exposing
    ( Element(..)
    , Stat(..)
    , StatTally
    , Stats
    , fromId
    , tallyClose
    , tallyInit
    , tallyMerge
    , tallyPush
    , toId
    )

import Dict as Dict exposing (Dict)



{-
   * sum stats:
     * from a single item (two affixes may affect the same stat. ex. poe: life + hybrid life/armor)
     * from multiple items
     * "sum" has different behavior for different stats. FlatDamage: (+), MoreDamage: (*)
   * render stats:
     * for a single affix (like poe shift-item-view)
     * for a single item (with summed stats)
     * for a character/multiple summed items (opt: break the sum down into sources, as in PoB or swarm2)
   * derived-stats, based on/distinct from raw-stats. examples:
     * damage = flat-damage * more-damage
     * life = flat-life * life%
     * fire-res = min(75, raw-fire-res)
-}


type Element
    = Fire
    | Lightning
    | Cold


type Stat
    = Life
    | Resist Element
    | FlatDamage
    | MoreDamage


statList : List Stat
statList =
    [ Life
    , Resist Fire
    , Resist Lightning
    , Resist Cold
    , FlatDamage
    , MoreDamage
    ]


toId : Stat -> String
toId stat =
    case stat of
        Life ->
            "life"

        Resist Fire ->
            "fireres"

        Resist Lightning ->
            "lightres"

        Resist Cold ->
            "coldres"

        FlatDamage ->
            "dmg"

        MoreDamage ->
            "dmgpct"


byId : Dict String Stat
byId =
    statList
        |> List.map (\stat -> ( toId stat, stat ))
        |> Dict.fromList


fromId : String -> Maybe Stat
fromId s =
    Dict.get s byId


type alias StatTallyData =
    Dict String (List Int)


type StatTally
    = StatTally StatTallyData


tallyInit : StatTally
tallyInit =
    Dict.empty |> StatTally


tallyPush : Stat -> Int -> StatTally -> StatTally
tallyPush stat val (StatTally tally) =
    tally |> Dict.update (toId stat) (Maybe.withDefault [] >> (::) val >> Just) |> StatTally


tallyMerge : StatTally -> StatTally -> StatTally
tallyMerge (StatTally da) (StatTally db) =
    Dict.merge
        Dict.insert
        (\key a b -> a ++ b |> Dict.insert key)
        Dict.insert
        da
        db
        Dict.empty
        |> StatTally


type alias Stats =
    { tallied : List Stat
    , life : Int
    , resist : { fire : Int, cold : Int, lightning : Int }
    , dmg : { flat : Int, pct : Int }
    }


tallyGet : Stat -> StatTallyData -> List Int
tallyGet stat =
    Dict.get (toId stat) >> Maybe.withDefault []


tallyClose : StatTally -> Stats
tallyClose (StatTally tally) =
    { tallied = tally |> Dict.keys |> List.map fromId |> List.filterMap identity
    , life = tally |> tallyGet Life |> List.sum
    , resist =
        { fire = tally |> tallyGet (Resist Fire) |> List.sum
        , cold = tally |> tallyGet (Resist Cold) |> List.sum
        , lightning = tally |> tallyGet (Resist Lightning) |> List.sum
        }
    , dmg =
        { flat = tally |> tallyGet FlatDamage |> List.product
        , pct = tally |> tallyGet MoreDamage |> List.product
        }
    }
