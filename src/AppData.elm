module AppData exposing (AppData, Lang, decodeJson, m)

import Dict as Dict exposing (Dict)
import Item.AffixBase as AffixBase exposing (AffixBase, AffixBases, AffixGroup, NonemptyList, toNonemptyList)
import Item.ArmorBase as ArmorBase exposing (ArmorBase)
import Item.WeaponBase as WeaponBase exposing (WeaponBase)
import Json.Decode as Json


type alias Lang =
    Dict String String


type alias AppData =
    { lang : Lang
    , weapons : NonemptyList WeaponBase
    , armors : NonemptyList ArmorBase
    , affixes : AffixBases
    }


decodeJson : Json.Decoder AppData
decodeJson =
    Json.map4 AppData
        (Json.field "Lang.en-us" decodeLang)
        (Json.field "WeaponBase" (WeaponBase.decodeJson |> Json.list |> Json.andThen (toNonemptyList "WeaponBase")))
        (Json.field "ArmorBase" (ArmorBase.decodeJson |> Json.list |> Json.andThen (toNonemptyList "ArmorBase")))
        (Json.field "AffixBase" AffixBase.decodeJson)
        |> Json.andThen (\val -> validate val |> maybeFail val)


maybeFail : a -> Maybe String -> Json.Decoder a
maybeFail val maybeError =
    case maybeError of
        Nothing ->
            Json.succeed val

        Just error ->
            Json.fail error


{-| Additional validation on a fully-decoded AppData.

We validate some stuff while it's decoded, but validating some data depends on
decoding other data (like language data), and it's more convenient to do those
at the end.

-}
validate : AppData -> Maybe String
validate appData =
    let
        -- only one language for now.
        lang =
            appData.lang

        expectedLangKeys : List String
        expectedLangKeys =
            [ appData.weapons.list |> List.map .baseId
            , appData.armors.list |> List.map .baseId
            , appData.affixes.all.prefixes.list |> List.map (.affixId >> (++) "prefix.title.")
            , appData.affixes.all.suffixes.list |> List.map (.affixId >> (++) "suffix.title.")
            ]
                |> List.concat

        maybeErrors : List (Maybe String)
        maybeErrors =
            [ expectedLangKeys |> List.map (validateLangKey lang)

            -- other validation? so far, just lang-key checks.
            ]
                |> List.concat
    in
    case maybeErrors |> List.filterMap identity of
        [] ->
            Nothing

        errors ->
            errors
                |> List.map ((++) "- ")
                |> String.join "\n"
                |> (++) ((errors |> List.length |> String.fromInt) ++ " appData errors:\n")
                |> Just


validateLangKey : Lang -> String -> Maybe String
validateLangKey lang key =
    if lang |> Dict.member key then
        Nothing

    else
        "missing lang-key: " ++ key |> Just


m : Lang -> String -> String
m lang key =
    lang
        |> Dict.get key
        |> Maybe.withDefault ("???" ++ key ++ "???")


decodeLangEntry : Json.Decoder ( String, String )
decodeLangEntry =
    Json.map2 Tuple.pair
        (Json.field "key" Json.string)
        (Json.field "val" Json.string)


decodeLang : Json.Decoder Lang
decodeLang =
    Json.list decodeLangEntry |> Json.map Dict.fromList
