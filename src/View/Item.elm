module View.Item exposing (item, view)

import AppData exposing (AppData, Lang, m)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Item as I exposing (Item)
import Item.Roll as R
import Lang as L
import Lang.Text as T
import Model as M exposing (Model, ModelData, Msg)
import Stat as Stat exposing (Stat, Stats)


view : ModelData -> Int -> Html Msg
view model index =
    let
        { lang } =
            model.appData

        body =
            case model.val |> List.drop index |> List.head of
                Nothing ->
                    [ text "no such item" ]

                Just i ->
                    item lang index i :: actions model.appData lang index i
    in
    div [ class "main" ]
        ([ div [] [ a [ href "/" ] [ text "Back to item list" ] ]
         , h3 [] [ text <| "Item " ++ String.fromInt index ]
         ]
            ++ body
        )


actions : AppData -> Lang -> Int -> Item -> List (Html Msg)
actions appData lang index itemType =
    let
        list =
            [ { event = R.FullReroll, label = "full reroll" }
            , { event = R.Whetstone, label = "whetstone" }
            , { event = R.Scouring, label = "scouring" }
            , { event = R.Unwhetstone, label = "unwhetstone" }
            , { event = R.Transmutation, label = "transmutation" }
            , { event = R.Augmentation, label = "augmentation" }
            , { event = R.Alteration, label = "alteration" }
            , { event = R.Regal, label = "regal" }
            , { event = R.Alchemy, label = "alchemy" }
            , { event = R.Chaos, label = "chaos" }
            , { event = R.Exalted, label = "exalted" }
            , { event = R.Annulment, label = "annulment" }
            ]

        --]
        filter { event } =
            R.isValid appData event itemType

        action { event, label } =
            div [] [ button [ onClick <| M.ItemRollReq index event ] [ text label ] ]
    in
    list |> List.filter filter |> List.map action


item : Lang -> Int -> Item -> Html msg
item lang index itemType =
    let
        url =
            "/item/" ++ String.fromInt index

        { cls, title, content } =
            case itemType of
                I.FlaskItem ->
                    { cls = [ "flask", "normal" ]
                    , title = "Flask"
                    , content = []
                    }

                I.WeaponItem ({ base, quality, rarity } as i) ->
                    { cls = [ "weapon", cssRarity rarity ]
                    , title = renderTitle lang i
                    , content =
                        [ div [] [ text base.type_ ]
                        , div [] [ text <| "Quality: " ++ String.fromInt quality ]
                        , div [] [ text <| "Damage: " ++ String.fromInt base.damage.min ++ " - " ++ String.fromInt base.damage.max ]
                        , div [] [ text <| "Speed: " ++ String.fromFloat (I.weaponAPS i) ]
                        , viewAffixes lang rarity
                        ]
                    }

                I.ArmorItem ({ base, quality, rarity } as i) ->
                    { cls = [ "armor", cssRarity rarity ]
                    , title = renderTitle lang i
                    , content =
                        [ div [] [ text base.type_ ]
                        , div [] [ text <| "Quality: " ++ String.fromInt quality ]
                        , div [] [ text <| "Defense: " ++ String.fromInt (I.armorDefense i) ]
                        , viewAffixes lang rarity
                        ]
                    }
    in
    div
        ([ class "item" ] ++ List.map class cls)
        [ div [ class "name" ] [ a [ href url ] [ text title ] ]
        , div [ class "content" ] content
        ]


renderTitle : Lang -> { a | base : { b | baseId : String }, rarity : I.Rarity } -> String
renderTitle lang { rarity, base } =
    case rarity of
        I.Normal ->
            base.baseId |> m lang

        I.Magic affix1 affix2 ->
            let
                baseName =
                    base.baseId |> m lang

                affixTitle whatfix =
                    List.head >> Maybe.map (.affixId >> (++) (whatfix ++ ".title.") >> m lang)

                ( suffix, prefix ) =
                    rarity
                        |> I.rarityAffixes
                        |> List.partition .isSuffix
                        |> Tuple.mapBoth (affixTitle "suffix") (affixTitle "prefix")
            in
            [ prefix, Just baseName, suffix ] |> List.filterMap identity |> String.join " "

        I.Rare _ ->
            -- TODO randomized rare names
            [ "Rare", base.baseId |> m lang ] |> String.join " "


cssRarity : I.Rarity -> String
cssRarity rarity =
    case rarity of
        I.Normal ->
            "normal"

        I.Magic _ _ ->
            "magic"

        I.Rare _ ->
            "rare"


viewAffixes : Lang -> I.Rarity -> Html msg
viewAffixes lang rarity =
    let
        stats =
            rarity |> I.rarityStats |> Stat.tallyClose
    in
    case stats.tallied |> List.map (viewStat lang stats) of
        [] ->
            div [ class "affixes", class "empty" ] []

        affixText ->
            div [ class "affixes", class "nonempty" ] affixText


viewStat : Lang -> Stats -> Stat -> Html msg
viewStat lang stats stat =
    -- div [] [ text <| affix.affixId ]
    -- div [] [ text <| Debug.toString stats ]
    div [] [ text <| L.m lang <| T.Stat stats stat ]
