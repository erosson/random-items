module View.List exposing (view)

import AppData exposing (Lang, m)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Item as I exposing (Item)
import Item.AffixBase as AffixBase exposing (AffixBase)
import Json.Decode as Json
import Lang as L
import Lang.Text as T
import Maybe.Extra
import Model as M exposing (Model, ModelData, Msg)
import Url as Url exposing (Url)
import View.Item


view : ModelData -> Html Msg
view model =
    let
        { lang } =
            model.appData
    in
    div [ class "main", class "ready" ]
        [ input
            [ type_ "number"
            , value (String.fromInt model.rollN)
            , onInput (M.RollN << Maybe.withDefault model.rollN << String.toInt)
            ]
            []
        , button [ onClick M.RerollAllReq ] [ text "roll" ]
        , div [] [ text <| L.m lang T.HelloWorld ]
        , div []
            (case model.val of
                [] ->
                    [ text "(empty)" ]

                items ->
                    items |> List.indexedMap (View.Item.item lang)
            )
        , div [] [ model.url |> Url.toString |> text ]
        ]
