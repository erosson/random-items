module Item exposing
    ( Armor
    , Item(..)
    , ItemBase(..)
    , Rarity(..)
    , Weapon
    , armorDefense
    , baseType
    , quality
    , rarity
    , rarityAffixes
    , rarityStats
    , setQuality
    , setRarity
    , weaponAPS
    )

import Item.AffixBase as AffixBase exposing (AffixBase, StatBase)
import Item.ArmorBase as ArmorBase exposing (ArmorBase)
import Item.WeaponBase as WeaponBase exposing (WeaponBase)
import Maybe.Extra
import Stat as Stat exposing (Stat, StatTally, Stats)


type ItemBase
    = WeaponBase WeaponBase
    | ArmorBase ArmorBase


type Rarity
    = Normal
    | Magic AffixBase (Maybe AffixBase)
    | Rare (List AffixBase)


type alias Weapon =
    { base : WeaponBase
    , quality : Int
    , rarity : Rarity
    }


type alias Armor =
    { base : ArmorBase
    , quality : Int
    , rarity : Rarity
    , defenseRoll : Float
    }


type Item
    = WeaponItem Weapon
    | ArmorItem Armor
    | FlaskItem -- TODO


weaponAPS : Weapon -> Float
weaponAPS item =
    toFloat item.base.speed / 10


armorDefense : Armor -> Int
armorDefense item =
    let
        range =
            item.base.defense

        dRange =
            range.ceil - range.floor
    in
    range.floor + (toFloat dRange * item.defenseRoll |> round)


rarityAffixes : Rarity -> List AffixBase
rarityAffixes r =
    case r of
        Normal ->
            []

        Magic affix1 affix2 ->
            affix1 :: Maybe.Extra.toList affix2

        Rare affixes ->
            affixes


rarityStats : Rarity -> StatTally
rarityStats =
    let
        fold : StatBase -> StatTally -> StatTally
        fold stat =
            -- TODO items don't yet roll for stats. for now, we always use the min possible rolled value.
            Stat.tallyPush stat.name stat.value.floor
    in
    rarityAffixes >> List.map .stats >> List.concat >> List.foldl fold Stat.tallyInit


type alias AnyBase b =
    { b | baseId : String, type_ : String }


baseType : Item -> Maybe String
baseType item =
    case item of
        WeaponItem i ->
            Just i.base.type_

        ArmorItem i ->
            Just i.base.type_

        FlaskItem ->
            Nothing


quality : Item -> Maybe Int
quality item =
    case item of
        WeaponItem i ->
            Just i.quality

        ArmorItem i ->
            Just i.quality

        FlaskItem ->
            Nothing


setQuality : Int -> Item -> Item
setQuality q item =
    case item of
        WeaponItem i ->
            WeaponItem { i | quality = q }

        ArmorItem i ->
            ArmorItem { i | quality = q }

        other ->
            other


rarity : Item -> Maybe Rarity
rarity item =
    case item of
        WeaponItem i ->
            Just i.rarity

        ArmorItem i ->
            Just i.rarity

        FlaskItem ->
            Nothing


setRarity : Rarity -> Item -> Item
setRarity r item =
    case item of
        WeaponItem i ->
            WeaponItem { i | rarity = r }

        ArmorItem i ->
            ArmorItem { i | rarity = r }

        other ->
            other
