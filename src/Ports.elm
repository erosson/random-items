port module Ports exposing (initAppData)

import Json.Decode as Json


port initAppData : (Json.Value -> msg) -> Sub msg
