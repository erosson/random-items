module Item.AffixBase exposing
    ( AffixBase
    , AffixBases
    , AffixGroup
    , Family
    , NonemptyList
    , StatBase
    , decodeJson
    , toNonemptyList
    )

import Dict as Dict exposing (Dict)
import Json.Decode as D
import Json.Decode.Pipeline as P
import Set as Set exposing (Set)
import Stat as Stat exposing (Stat)


type alias IntRoll =
    { floor : Int, ceil : Int }


type alias Family =
    String


type alias AffixBase =
    { affixId : String
    , isSuffix : Bool
    , tier : Int
    , ilvl : Int
    , rollGroup : Set String
    , families : Set Family
    , stats : List StatBase
    }


type alias StatBase =
    { name : Stat, value : IntRoll }


decodeBase : D.Decoder AffixBase
decodeBase =
    D.map7 AffixBase
        (D.field "affixId" D.string)
        (D.field "isSuffix" D.bool)
        (D.field "tier" D.int)
        (D.field "ilvl" D.int)
        (D.map (Set.fromList << String.split ",")
            (D.field "rollGroup" D.string)
        )
        (D.map (Set.fromList << String.split ",")
            (D.field "families" D.string)
        )
        (D.map2 StatBase
            (D.field "stat" decodeStat)
            (D.map2 IntRoll
                (D.field "value.floor" D.int)
                (D.field "value.ceil" D.int)
            )
            -- TODO eventually we'll support parsing affixes with more than one stat
            |> D.map List.singleton
        )


decodeStat : D.Decoder Stat
decodeStat =
    let
        fromId id =
            case id of
                Nothing ->
                    D.fail "no such stat"

                Just stat ->
                    D.succeed stat
    in
    D.string |> D.map Stat.fromId |> D.andThen fromId


type alias NonemptyList a =
    { head : a
    , tail : List a
    , list : List a
    }


toNonemptyList : String -> List a -> D.Decoder (NonemptyList a)
toNonemptyList context list =
    case list of
        [] ->
            D.fail ("non-empty list required: " ++ context)

        head :: tail ->
            D.succeed { head = head, tail = tail, list = list }


type alias AffixGroup =
    { prefixes : NonemptyList AffixBase
    , suffixes : NonemptyList AffixBase
    , all : NonemptyList AffixBase
    }


byGroup : List AffixBase -> Dict String (List AffixBase)
byGroup =
    let
        eachAffix affix grouped =
            Set.foldl (eachGroup affix) grouped affix.rollGroup

        eachGroup affix group =
            Dict.update group (Maybe.withDefault [] >> (::) affix >> Just)
    in
    List.foldl eachAffix Dict.empty


decodeGroup : String -> List AffixBase -> D.Decoder AffixGroup
decodeGroup groupName affixes =
    let
        ( suffixes, prefixes ) =
            affixes |> List.partition .isSuffix
    in
    D.map3 AffixGroup
        (toNonemptyList (groupName ++ ":prefix") prefixes)
        (toNonemptyList (groupName ++ ":suffix") suffixes)
        (toNonemptyList (groupName ++ ":all") affixes)


type alias AffixBases =
    { all : AffixGroup, byGroup : Dict String AffixGroup }


decodeGroups : List AffixBase -> D.Decoder (Dict String AffixGroup)
decodeGroups =
    let
        listsToAffixGroups : Dict String (List AffixBase) -> D.Decoder (Dict String AffixGroup)
        listsToAffixGroups =
            Dict.foldl eachGroup (D.succeed Dict.empty)

        eachGroup : String -> List AffixBase -> D.Decoder (Dict String AffixGroup) -> D.Decoder (Dict String AffixGroup)
        eachGroup groupName affixes =
            D.andThen (\groups -> decodeGroup groupName affixes |> D.map (\axs -> Dict.insert groupName axs groups))
    in
    byGroup >> listsToAffixGroups


decodeJson : D.Decoder AffixBases
decodeJson =
    decodeBase
        |> D.list
        |> D.andThen
            (\list ->
                D.map2 AffixBases
                    (decodeGroup "__all__" list)
                    (decodeGroups list)
            )
