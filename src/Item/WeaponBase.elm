module Item.WeaponBase exposing (WeaponBase, decodeJson)

import Json.Decode as D


type alias IntRange =
    { min : Int, max : Int }


type alias Requires =
    { level : Int }


type alias WeaponBase =
    { baseId : String

    -- , implicit : Affix
    , type_ : String
    , requires : Requires
    , speed : Int
    , range : Int
    , damage : IntRange
    }


decodeJson : D.Decoder WeaponBase
decodeJson =
    D.map6 WeaponBase
        (D.field "baseId" D.string)
        (D.field "type" D.string)
        (D.map Requires
            (D.field "requires.level" D.int)
        )
        (D.field "speed" D.int)
        (D.field "range" D.int)
        (D.map2 IntRange
            (D.field "damage.min" D.int)
            (D.field "damage.max" D.int)
        )
