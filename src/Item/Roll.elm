module Item.Roll exposing (Msg(..), generator, isValid, rollItems)

import AppData as AppData exposing (AppData)
import Dict as Dict exposing (Dict)
import Item as Item exposing (Item(..), ItemBase(..), Rarity(..))
import Item.AffixBase as AffixBase exposing (AffixBase, AffixGroup, StatBase)
import Item.ArmorBase as ArmorBase exposing (ArmorBase)
import Item.WeaponBase as WeaponBase exposing (WeaponBase)
import Maybe.Extra
import Random
import Random.Extra
import Random.List
import Set as Set exposing (Set)


type Msg
    = FullReroll
    | Whetstone
    | Unwhetstone
    | Scouring
    | Transmutation
    | Augmentation
    | Alteration
    | Regal
    | Alchemy
    | Chaos
    | Exalted
    | Annulment


generator : AppData -> Msg -> Item -> Result String (Random.Generator Item)
generator appData msg item =
    case msg of
        FullReroll ->
            rollItem appData |> Ok

        Whetstone ->
            whetstone 1 item

        Unwhetstone ->
            whetstone -1 item

        Scouring ->
            scouring item

        Transmutation ->
            transmutation appData item

        Augmentation ->
            augmentation appData item

        Alteration ->
            alteration appData item

        Regal ->
            regal appData item

        Alchemy ->
            alchemy appData item

        Chaos ->
            chaos appData item

        Exalted ->
            exalted appData item

        Annulment ->
            annulment item


isValid : AppData -> Msg -> Item -> Bool
isValid appData msg =
    generator appData msg >> Result.toMaybe >> Maybe.Extra.isJust


transmutation : AppData -> Item -> Result String (Random.Generator Item)
transmutation appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just Normal, Just baseType ) ->
            rollMagicRarity appData baseType
                |> Random.map (\r -> Item.setRarity r item)
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be normal rarity."


augmentation : AppData -> Item -> Result String (Random.Generator Item)
augmentation appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just (Magic affix1 (Just _)), Just baseType ) ->
            Err "Item must be magic rarity with only one affix."

        ( Just (Magic affix1 Nothing), Just baseType ) ->
            appData.affixes.byGroup
                |> Dict.get baseType
                |> Maybe.Extra.unwrap (Random.constant Nothing) (\group -> rollMagicAffix2 group affix1)
                |> Random.map (Magic affix1 >> (\r -> Item.setRarity r item))
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be magic rarity."


alteration : AppData -> Item -> Result String (Random.Generator Item)
alteration appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just (Magic _ _), Just baseType ) ->
            rollMagicRarity appData baseType
                |> Random.map (\r -> Item.setRarity r item)
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be magic rarity."


regal : AppData -> Item -> Result String (Random.Generator Item)
regal appData item =
    case item |> Item.rarity of
        Nothing ->
            Err "Item has no rarity."

        Just ((Magic _ _) as rarity) ->
            -- make the item rare (with same affixes), then add an affix
            item
                |> Item.setRarity (Rare (rarity |> Item.rarityAffixes))
                |> exalted appData

        Just _ ->
            Err "Item must be magic rarity."


alchemy : AppData -> Item -> Result String (Random.Generator Item)
alchemy appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just Normal, Just baseType ) ->
            rollRareRarity appData baseType
                |> Random.map (\r -> Item.setRarity r item)
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be normal rarity."


chaos : AppData -> Item -> Result String (Random.Generator Item)
chaos appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just (Rare _), Just baseType ) ->
            rollRareRarity appData baseType
                |> Random.map (\r -> Item.setRarity r item)
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be rare rarity."


exalted : AppData -> Item -> Result String (Random.Generator Item)
exalted appData item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just (Rare affixes), Just baseType ) ->
            if List.length affixes >= 6 then
                Err "Item has the maximum number of affixes."

            else
                appData.affixes.byGroup
                    |> Dict.get baseType
                    |> Maybe.Extra.unwrap (Random.constant Nothing) (\group -> rollNextRareAffix group affixes)
                    |> Random.map (Maybe.Extra.toList >> (++) affixes >> Rare >> (\r -> Item.setRarity r item))
                    |> Ok

        ( Just _, Just base ) ->
            Err "Item must be rare rarity."


annulment : Item -> Result String (Random.Generator Item)
annulment item =
    case ( item |> Item.rarity, item |> Item.baseType ) of
        ( Nothing, _ ) ->
            Err "Item has no rarity."

        ( _, Nothing ) ->
            Err "Item has no base."

        ( Just (Rare []), Just baseType ) ->
            Err "Item has no affixes."

        ( Just (Rare affixes), Just baseType ) ->
            affixes
                |> Random.List.choose
                |> Random.map
                    (Tuple.second
                        >> Rare
                        >> (\r -> Item.setRarity r item)
                    )
                |> Ok

        ( Just _, Just base ) ->
            Err "Item must be rare rarity."


whetstone : Int -> Item -> Result String (Random.Generator Item)
whetstone diff item =
    case Item.quality item of
        Nothing ->
            Err "Item does not have quality."

        Just pre ->
            let
                cap =
                    max 20 pre

                -- don't cap to lower than the start value
                post =
                    pre + diff |> max 0 |> min cap
            in
            if pre == post then
                Err "Item already has maximum quality."

            else
                item |> Item.setQuality post |> Random.constant |> Ok


scouring : Item -> Result String (Random.Generator Item)
scouring item =
    case item of
        FlaskItem ->
            Err "Cannot scour flasks."

        WeaponItem w ->
            if w.rarity == Normal then
                Err "Cannot scour normal item."

            else
                Ok <| Random.constant <| WeaponItem { w | rarity = Normal }

        ArmorItem a ->
            if a.rarity == Normal then
                Err "Cannot scour normal item."

            else
                Ok <| Random.constant <| ArmorItem { a | rarity = Normal }



-- internal operations


rollItems : Int -> AppData -> Random.Generator (List Item)
rollItems n appData =
    Random.list n (rollMaybeItem appData)
        |> Random.map (List.filterMap identity)


rollMaybeItem : AppData -> Random.Generator (Maybe Item)
rollMaybeItem appData =
    Random.Extra.frequency
        ( 2, Random.constant Nothing )
        [ ( 1, rollItem appData |> Random.map Just ) ]


rollItem : AppData -> Random.Generator Item
rollItem appData =
    Random.Extra.frequency
        ( 1, Random.constant FlaskItem )
        [ ( 4, rollWeapon appData |> Random.map WeaponItem )
        , ( 4, rollArmor appData |> Random.map ArmorItem )
        ]


rollQuality : Random.Generator Int
rollQuality =
    Random.Extra.frequency
        ( 10, Random.constant 0 )
        [ ( 3, Random.int 1 20 )
        ]


rollRarity : AppData -> String -> Random.Generator Rarity
rollRarity appData rollGroup =
    Random.Extra.frequency
        ( 10, Random.constant Normal )
        [ ( 3, rollMagicRarity appData rollGroup )
        , ( 1, rollRareRarity appData rollGroup )
        ]


rollMagicRarity : AppData -> String -> Random.Generator Rarity
rollMagicRarity appData rollGroup =
    appData.affixes.byGroup
        |> Dict.get rollGroup
        |> Maybe.map (rollMagicAffixes >> Random.map (\( a, b ) -> Magic a b))
        |> Maybe.withDefault (Random.constant Normal)


rollRareRarity : AppData -> String -> Random.Generator Rarity
rollRareRarity appData rollGroup =
    appData.affixes.byGroup
        |> Dict.get rollGroup
        |> Maybe.map rollRareAffixes
        |> Maybe.withDefault (Random.constant [])
        |> Random.map Rare


{-| Choose a random item, then use the chosen item to further filter the list.

Like Random.List.choose, but remove multiple elements from future choices.

-}
chooseAndFilter : (a -> a -> Bool) -> List a -> Random.Generator ( Maybe a, List a )
chooseAndFilter pred =
    let
        andFilter ( maybeItem, nextItems ) =
            let
                -- The filter predicate's based on the chosen item (if any).
                filter =
                    maybeItem |> Maybe.Extra.unwrap identity (pred >> List.filter)
            in
            ( maybeItem, nextItems |> filter )
    in
    Random.List.choose >> Random.map andFilter


{-| Two familied-things can be together if they have no families in common.

Just like in PoE. For example, life affixes are all part of the same family -
you can't roll 3 life affixes of different tiers. This function's more general
than item affixes, though - also works for boss-monster affixes, for example.

-}
isFamiliesValid : (a -> Set AffixBase.Family) -> a -> a -> Bool
isFamiliesValid getFamilies a b =
    Set.intersect (getFamilies a) (getFamilies b) |> Set.isEmpty


isAffixFamiliesValid : AffixBase -> AffixBase -> Bool
isAffixFamiliesValid =
    isFamiliesValid .families


chooseUpToN : (a -> a -> Bool) -> Int -> List a -> Random.Generator (List a)
chooseUpToN pred =
    -- instead of simply recursing chooseUpToN, this version with an accumulator
    -- (`picked`) allows tail call optimization.
    let
        loop picked n items =
            if n <= 0 then
                -- We've picked all n items.
                picked

            else
                -- choose one item and loop.
                items
                    -- |> Random.List.choose
                    |> chooseAndFilter pred
                    |> Random.andThen
                        (\( maybeItem, nextItems ) ->
                            case maybeItem of
                                Nothing ->
                                    -- We've picked every item - list.length < n. Quit early.
                                    picked

                                Just item ->
                                    loop (picked |> Random.map ((::) item)) (n - 1) nextItems
                        )
    in
    loop (Random.constant [])


chooseAffixes : Int -> List AffixBase -> Random.Generator (List AffixBase)
chooseAffixes =
    chooseUpToN isAffixFamiliesValid


rollMagicAffix2 : AffixGroup -> AffixBase -> Random.Generator (Maybe AffixBase)
rollMagicAffix2 possible affix1 =
    let
        group =
            if affix1.isSuffix then
                possible.prefixes

            else
                possible.suffixes

        filter =
            isAffixFamiliesValid affix1
    in
    chooseAffixes 1 (group.list |> List.filter filter)
        |> Random.map List.head


rollNextRareAffix : AffixGroup -> List AffixBase -> Random.Generator (Maybe AffixBase)
rollNextRareAffix possible affixes =
    let
        group =
            possible.all

        filter nextAffix =
            List.all (isAffixFamiliesValid nextAffix) affixes
    in
    chooseAffixes 1 (group.list |> List.filter filter)
        |> Random.map List.head


rollMagicAffixes : AffixGroup -> Random.Generator ( AffixBase, Maybe AffixBase )
rollMagicAffixes affixes =
    let
        maybeRollAffix2 affix1 =
            Random.Extra.maybe (Random.Extra.oneIn 3) (rollMagicAffix2 affixes affix1)
                |> Random.map (Maybe.Extra.join >> Tuple.pair affix1)
    in
    Random.andThen maybeRollAffix2
        -- (chooseAffixes 1 affixes.all.list)
        (Random.uniform affixes.all.head affixes.all.tail)


rollRareAffixes : AffixGroup -> Random.Generator (List AffixBase)
rollRareAffixes affixes =
    -- deliberately ignore prefixes/suffixes on rares, because it's super
    -- annoying in poe and it works fine in d3
    -- TODO: unify this with rollNextRareAffix
    Random.int 2 6 |> Random.andThen (\n -> chooseAffixes n affixes.all.list)


roll01 : Random.Generator Float
roll01 =
    Random.float 0 1


rollWeapon : AppData -> Random.Generator Item.Weapon
rollWeapon appData =
    let
        roll1 : WeaponBase -> Random.Generator Item.Weapon
        roll1 base =
            Random.map2 (Item.Weapon base)
                rollQuality
                (rollRarity appData base.type_)
    in
    Random.andThen roll1
        (rollWeaponBase appData)


rollWeaponBase : AppData -> Random.Generator WeaponBase
rollWeaponBase appData =
    Random.uniform appData.weapons.head appData.weapons.tail


rollArmor : AppData -> Random.Generator Item.Armor
rollArmor appData =
    let
        roll1 : ArmorBase -> Random.Generator Item.Armor
        roll1 base =
            Random.map3 (Item.Armor base)
                rollQuality
                (rollRarity appData base.type_)
                roll01
    in
    Random.andThen roll1
        (rollArmorBase appData)


rollArmorBase : AppData -> Random.Generator ArmorBase
rollArmorBase appData =
    Random.uniform appData.armors.head appData.armors.tail
