module Item.ArmorBase exposing (ArmorBase, decodeJson)

import Json.Decode as D


type alias IntRoll =
    { floor : Int, ceil : Int }


type alias Requires =
    { level : Int }


type alias ArmorBase =
    { baseId : String

    -- , implicit : Affix
    , type_ : String
    , requires : Requires
    , defense : IntRoll
    }


decodeJson : D.Decoder ArmorBase
decodeJson =
    D.map4 ArmorBase
        (D.field "baseId" D.string)
        (D.field "type" D.string)
        (D.map Requires
            (D.field "requires.level" D.int)
        )
        (D.map2 IntRoll
            (D.field "defense.floor" D.int)
            (D.field "defense.ceil" D.int)
        )
