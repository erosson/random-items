module Lang.En exposing (m)

import Lang.Text as Text exposing (Text(..))
import Stat as S exposing (..)


m : Text -> String
m text =
    case text of
        HelloWorld ->
            "hello world!"

        Stat stats stat ->
            case stat of
                Life ->
                    "+" ++ String.fromInt stats.life ++ " maximum life"

                Resist Fire ->
                    "+" ++ String.fromInt stats.resist.fire ++ "% fire resistance"

                Resist Cold ->
                    "+" ++ String.fromInt stats.resist.cold ++ "% cold resistance"

                Resist Lightning ->
                    "+" ++ String.fromInt stats.resist.lightning ++ "% lightning resistance"

                FlatDamage ->
                    "+" ++ String.fromInt stats.dmg.flat ++ " damage"

                MoreDamage ->
                    String.fromInt stats.dmg.pct ++ "% more damage"
