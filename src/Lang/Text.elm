module Lang.Text exposing (Text(..))

import Model
import Stat as Stat exposing (Stat, Stats)


type Text
    = HelloWorld
    | Stat Stats Stat
