module Model exposing
    ( Flags
    , Model(..)
    , ModelData
    , Msg(..)
    , init
    , subscriptions
    , update
    )

import AppData as AppData exposing (AppData)
import Browser
import Browser.Navigation as Nav
import Item as Item exposing (Item)
import Item.ArmorBase as ArmorBase exposing (ArmorBase)
import Item.Roll
import Item.WeaponBase as WeaponBase exposing (WeaponBase)
import Json.Decode as Json
import List.Extra
import Ports
import Random
import Url as Url exposing (Url)


type Model
    = PendingModel PendingData
    | ErrorModel PendingData String
    | ReadyModel ModelData


type alias PendingData =
    { url : Url
    , nav : Nav.Key
    }


type alias ModelData =
    { appData : AppData
    , url : Url
    , nav : Nav.Key
    , rollN : Int
    , val : List Item
    }


type Msg
    = InitAppData Json.Value
    | RollN Int
    | RerollAllReq
    | RerollAllRes (List Item)
    | OnUrlChange Url
    | OnUrlRequest Browser.UrlRequest
    | ItemRollReq Int Item.Roll.Msg
    | ItemRollRes Int Item


type alias Flags =
    ()


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url nav =
    ( PendingModel { url = url, nav = nav }, Cmd.none )


initReady : Json.Value -> PendingData -> ( Model, Cmd Msg )
initReady appDataJson pending =
    case appDataJson |> Json.decodeValue AppData.decodeJson of
        Err fail ->
            ( fail
                |> Json.errorToString
                |> (++) "failed to decode app-data: "
                |> ErrorModel pending
            , Cmd.none
            )

        Ok appData ->
            let
                model =
                    { appData = appData
                    , url = pending.url
                    , nav = pending.nav
                    , val = []
                    , rollN = 10
                    }
            in
            ( ReadyModel model, rollItems model )


rollItems : ModelData -> Cmd Msg
rollItems model =
    Item.Roll.rollItems model.rollN model.appData |> Random.generate RerollAllRes


pendingData : Model -> PendingData
pendingData m =
    case m of
        PendingModel pending ->
            pending

        ErrorModel pending error ->
            pending

        ReadyModel model ->
            { url = model.url, nav = model.nav }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        InitAppData data ->
            initReady data (pendingData model)

        OnUrlRequest (Browser.Internal url) ->
            ( model, url |> Url.toString |> Nav.pushUrl (pendingData model).nav )

        OnUrlRequest (Browser.External urlStr) ->
            ( model, urlStr |> Nav.load )

        _ ->
            case model of
                PendingModel pending ->
                    case msg of
                        OnUrlChange url ->
                            ( PendingModel { pending | url = url }, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                ErrorModel pending error ->
                    case msg of
                        OnUrlChange url ->
                            ( ErrorModel { pending | url = url } error, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                ReadyModel modelData ->
                    updateReady msg modelData
                        |> Tuple.mapFirst ReadyModel


updateItem : Item.Roll.Msg -> Item -> Random.Generator Item
updateItem msg item =
    Random.constant item


updateReady : Msg -> ModelData -> ( ModelData, Cmd Msg )
updateReady msg model =
    case msg of
        RerollAllReq ->
            ( model, rollItems model )

        RerollAllRes val ->
            ( { model | val = val }, Cmd.none )

        RollN val ->
            ( { model | rollN = val }, Cmd.none )

        OnUrlChange url ->
            ( { model | url = url }, Cmd.none )

        ItemRollReq index act ->
            case model.val |> List.drop index |> List.head of
                Nothing ->
                    ( model, Cmd.none )

                Just item ->
                    case item |> Item.Roll.generator model.appData act of
                        Err err ->
                            ( model, Cmd.none )

                        Ok gen ->
                            ( model, gen |> Random.generate (ItemRollRes index) )

        ItemRollRes index item ->
            ( { model | val = model.val |> List.Extra.setAt index item }, Cmd.none )

        -- everything below should've been handled earlier, in update
        InitAppData _ ->
            ( model, Cmd.none )

        OnUrlRequest _ ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Ports.initAppData InitAppData
        ]
