import './main.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';
import xlsx from 'xlsx'

const app = Elm.Main.init({
  node: document.getElementById('root')
});

registerServiceWorker();

const withLog = prefix => val => {
  console.log(prefix, val)
  return val
}
// Fetch the game data spreadsheet and transform it to JSON.
// https://github.com/SheetJS/js-xlsx/tree/master/demos/xhr#fetch
fetch('/app-data.ods')
.then(res => res.arrayBuffer())
.then(body => new Uint8Array(body))
.then(array => xlsx.read(array, {type: 'array'}))
.then(wb =>
  Object.keys(wb.Sheets)
  // The app ignores sheets named like "_name". I might be doing game balance
  // calculations or other stuff unused by the app there.
  .filter(name => !name.startsWith('_'))
  // https://www.npmjs.com/package/xlsx#utility-functions
  // https://www.npmjs.com/package/xlsx#json
  .reduce((data, name) => ({
    ...data,
    [name]: xlsx.utils.sheet_to_json(wb.Sheets[name]),
  }), {})
)
.then(withLog('app-data'))
.then(data => app.ports.initAppData.send(data))
.catch(console.error)
