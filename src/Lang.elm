module Lang exposing (m)

import AppData as AppData exposing (Lang)
import Lang.En as En
import Lang.Text exposing (Text)


m : Lang -> Text -> String
m _ =
    -- TODO lookup language in appdata
    let
        lang =
            "en"
    in
    case lang of
        "en" ->
            En.m

        _ ->
            En.m
