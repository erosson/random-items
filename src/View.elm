module View exposing (view)

import Browser
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Model as M exposing (Model, ModelData, Msg)
import Route as Route exposing (Route(..))
import Url as Url exposing (Url)
import View.Item
import View.List


view : Model -> Browser.Document Msg
view model =
    { title = "RNG, wee"
    , body = body model
    }


body : Model -> List (Html Msg)
body model =
    case model of
        M.PendingModel _ ->
            [ div [ class "main", class "pending" ] [ text "loading..." ] ]

        M.ErrorModel _ error ->
            [ div [ class "main", class "error" ]
                [ h1 [] [ text "oops!" ]
                , pre [] [ text error ]
                ]
            ]

        M.ReadyModel data ->
            ready data


ready : ModelData -> List (Html Msg)
ready model =
    case model.url |> Route.parse of
        Home ->
            [ View.List.view model ]

        Item index ->
            [ View.Item.view model index ]

        NotFound ->
            [ div [ class "main", class "error", class "notfound" ] [ text "404" ] ]
