module Route exposing (Route(..), parse, toString)

import Maybe.Extra
import Url as Url exposing (Url)
import Url.Parser as P exposing (..)


type Route
    = Home
    | Item Int
    | NotFound


routes : Parser (Route -> a) a
routes =
    oneOf
        [ map Home top
        , map Item (s "item" </> int)
        ]


parse : Url -> Route
parse =
    P.parse routes >> Maybe.withDefault NotFound


fromString : String -> Route
fromString =
    Url.fromString >> Maybe.Extra.unwrap NotFound parse


toString : Route -> String
toString route =
    case route of
        Home ->
            "/"

        Item index ->
            "/item/" ++ String.fromInt index

        NotFound ->
            "/404"
